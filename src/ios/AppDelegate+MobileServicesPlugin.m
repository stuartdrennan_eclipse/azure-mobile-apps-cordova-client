// ----------------------------------------------------------------------------
// Copyright (c) Microsoft Corporation. All rights reserved.
// ----------------------------------------------------------------------------
#import "AppDelegate+MobileServicesPlugin.h"
#import "MobileServicesPlugin.h"

@implementation AppDelegate (MobileServicesPlugin)

static MSClient *_msClient;

- (MSClient *)msClient
{
    return _msClient;
}

- (void)setMSClient:(MSClient *)msClient
{
    _msClient = msClient;
}

@end
